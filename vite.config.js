import { defineConfig, loadEnv } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';
const fs = require('node:fs');

export default ({ mode }) => {
    process.env = {...process.env, ...loadEnv(mode, process.cwd())}

    return defineConfig({
        server: {
            https: {
                key: fs.readFileSync(process.env.VITE_DEV_SSL_KEY_PATH),
                cert: fs.readFileSync(process.env.VITE_DEV_SSL_CRT_PATH)
            },
            host: process.env.VITE_APP_HOST,
        },
        plugins: [
            laravel({
                input: [
                    'resources/js/Backend/app.js',
                    'resources/js/Frontend/app.js',
                    'resources/themes/sneat/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css',
                    'resources/themes/sneat/assets/vendor/fonts/boxicons.css',
                    'resources/themes/sneat/assets/vendor/css/core.css',
                    'resources/themes/sneat/assets/vendor/css/theme-default.css',
                    'resources/themes/sneat/assets/vendor/css/pages/page-auth.css',
                    'resources/themes/sneat/assets/css/demo.css',
                    'resources/themes/sneat/assets/vendor/js/helpers.js',
                    'resources/themes/sneat/assets/js/config.js',
                    'resources/themes/sneat/assets/vendor/libs/jquery/jquery.js',
                    'resources/themes/sneat/assets/vendor/libs/popper/popper.js',
                    'resources/themes/sneat/assets/vendor/js/bootstrap.js',
                    'resources/themes/sneat/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js',
                    'resources/themes/sneat/assets/vendor/js/menu.js',
                    'resources/themes/sneat/assets/js/main.js'
                ],
                ssr: 'resources/js/Frontend/ssr.js',
                refresh: true,
            }),
            vue({
                template: {
                    transformAssetUrls: {
                        base: null,
                        includeAbsolute: false,
                    },
                },
            }),
        ],
        ssr: {
            noExternal: ['@inertiajs/server'],
        },
    })
}
