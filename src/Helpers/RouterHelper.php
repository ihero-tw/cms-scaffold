<?php
use Illuminate\Support\Str;

if (!function_exists('packagePath')) {

    /**
     *
     * @param String $siteNo
     * @return Mixed
     */
    function packagePath(String $filePath)
    {
        if (file_exists(base_path('packages')) && is_dir(base_path('packages'))) {
            return sprintf('%s/%s', base_path('packages'), $filePath);
        } elseif (file_exists(base_path('vendor/ihero')) && is_dir(base_path('vendor/ihero'))) {
            return sprintf('%s/%s', base_path('vendor/ihero'), $filePath);
        }
        return null;
    }
}
