<?php
use Illuminate\Support\Str;

if (!function_exists('generatorNo')) {

    /**
     *
     * @param String $siteNo
     * @return String
     */
    function generatorNo(String $prefix): String
    {
        $string = rand();
        $string = substr($string, 0, 4);
        $year = (int) date('Y') - 1912;

        return sprintf('%s%s%s%s', $prefix, $year, date('md'), str_pad($string, 4, '0', STR_PAD_LEFT));
    }
}


if (!function_exists('hashIdentifier')) {

    /**
     *
     * @param String $length
     * @return String
     */
    function hashIdentifier(String $length): String
    {
        return substr(bin2hex(Str::random($length)), $length);
    }
}

if (! function_exists('underscoredArrayKeys')) {
    /**
     * CamelCase to underscore
     *
     * Note:目前單一 Key值僅限一個大寫字母
     *
     * @param array $input
     * @return void
     */
    function underscoredArrayKeys(Array $input)
    {
        $keys = array_keys($input);

        for ($i=0; $i < count($keys); $i++) {
            $key = $keys[$i];
            if (preg_match('/[A-Z]/', $key, $match)) {
                $newKey = preg_replace('/[A-Z]/', '_'.strtolower($match[0]), $key, 1);
                $input[$newKey] = $input[$key];
                unset($input[$key]);
            };
        }

        return $input;
    }
}
