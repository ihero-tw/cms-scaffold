<?php

namespace App\Services;

use App\Repositories\ForceUpdateDateTimeRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ActivityLogService
{
    /**
     * __construct
     *
     * @param ForceUpdateDateTimeRepository $forceUpdateDateTimeRepository
     */
    public function __construct(
        ForceUpdateDateTimeRepository $forceUpdateDateTimeRepository
    ) {
        $this->forceUpdateDateTimeRepository = $forceUpdateDateTimeRepository;
    }

    /**
     * @param Model $model
     * @param string $type
     * @return void
     */
    public function log(Model $model, String $type = 'updated'): void
    {
        activity('presents')
            ->performedOn($model)
            ->causedBy(Auth::user())
            ->log($type);

        $this->forceUpdateDateTimeRepository->updateDateTime($model);
    }
}
