<?php

namespace Ihero\CMS\Scaffold\Macros;

use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Ihero\CMS\Scaffold\Macros\Contracts\MacroInterface;

class CollectionMacro implements MacroInterface
{
    public function register()
    {
        if (! Collection::hasMacro('paginate')) {
            Collection::macro('paginate', function ($perPage, $total = null, $page = null, $pageName = 'page') {
                $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);

                return new LengthAwarePaginator(
                    $this->forPage($page, $perPage),
                    $total ?: $this->count(),
                    $perPage,
                    $page,
                    [
                        'path' => LengthAwarePaginator::resolveCurrentPath(),
                        'pageName' => $pageName,
                    ]
                );
            });
        }
    }
}
