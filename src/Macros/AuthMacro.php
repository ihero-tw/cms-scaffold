<?php

namespace Ihero\CMS\Scaffold\Macros;

use Illuminate\Support\Facades\Auth;
use Ihero\CMS\Scaffold\Macros\Contracts\MacroInterface;

class AuthMacro implements MacroInterface
{
    public function register()
    {
        if (! Auth::hasMacro('organization')) {
            Auth::macro('organization', function () {
                if (Auth::guard(config('fortify.guard'))->check()) {
                    return Auth::guard(config('fortify.guard'))->user()->currentTeam->organization;
                }
                return null;
            });
        }
    }
}
