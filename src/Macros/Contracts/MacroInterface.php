<?php

namespace Ihero\CMS\Scaffold\Macros\Contracts;

interface MacroInterface
{
    public function register();
}
