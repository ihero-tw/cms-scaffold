<?php

namespace Ihero\CMS\Scaffold\Macros;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Ihero\CMS\Scaffold\Macros\Contracts\MacroInterface;

class RequestMacro implements MacroInterface
{
    public function register()
    {
        if (! Request::hasMacro('hostIs')) {
            Request::macro('hostIs', function ($type) {
                return $type == 'web' ?
                    request()->site instanceof Site:
                    Str::is(config('app.domains.'.$type), $this->host());
            });
        }
    }
}
