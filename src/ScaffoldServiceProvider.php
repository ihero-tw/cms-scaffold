<?php

namespace Ihero\CMS\Scaffold;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Ihero\CMS\Scaffold\Support\Host\Host;

class ScaffoldServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigs();
        $this->laodMacros();
        $this->loadSingleton();
        $this->loadHelpers();
    }

    /**
     *
     * @return void
     */
    public function mergeConfigs()
    {
        //
    }

    /**
     *
     * @return void
     */
    public function laodMacros()
    {
        foreach (glob(__DIR__ . '/Macros/*.php') as $file) {
            $class = 'Ihero\\CMS\\Scaffold\\Macros\\' . basename($file, '.php');
            if (class_exists($class)) {
                (App::make($class)->register());
            }
        }
    }

    /**
     *
     * @return void
     */
    public function loadSingleton()
    {
        $this->app->singleton('host', function ($app) {
            return new Host($app);
        });
    }

    /**
     *
     * @return void
     */
    public function loadHelpers()
    {
        foreach (glob(__DIR__ . '/Helpers/*.php') as $file) {
            require_once($file);
        }
        foreach (glob(app_path() . '/Helpers/*.php') as $file) {
            require_once($file);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'host'
        ];
    }

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->commands('Ihero\CMS\Scaffold\Console\Commands\InstallCommand');

        $this->publishes([
            __DIR__.'/Providers/' => app_path('Providers'),
        ], 'ih-cms-provider');
        $this->publishes([
            __DIR__.'/../resources/themes/' => resource_path('themes'),
        ], 'ih-cms-themes');
        $this->publishes([
            __DIR__.'/../resources/css/' => resource_path('css'),
        ], 'ih-cms-css');
        $this->publishes([
            __DIR__.'/../resources/js/' => resource_path('js'),
        ], 'ih-cms-js');
        $this->publishes([
            __DIR__.'/../resources/views/' => resource_path('views'),
        ], 'ih-cms-views');
        $this->publishes([
            __DIR__.'/../config/' => base_path('config'),
        ], 'ih-cms-config');
        $this->publishes([
            __DIR__.'/../routes/' => base_path('routes'),
        ], 'ih-cms-routes');
        $this->publishes([
            __DIR__.'/../.editorconfig' => base_path('.editorconfig'),
            __DIR__.'/../vite.config.js' => base_path('vite.config.js'),
            __DIR__.'/../database.dbml' => base_path('database.dbml')
        ], 'ih-cms-init');
    }
}
