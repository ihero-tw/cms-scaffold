<?php

namespace Ihero\CMS\Scaffold\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.cms:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CMS scaffolding.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm("Executing this command will overwrite files, Are you sure to execute?", true)) {
            $this->call('vendor:publish', ['--tag' => 'ih-cms-provider', '--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'ih-cms-themes', '--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'ih-cms-config', '--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'ih-cms-views', '--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'ih-cms-init', '--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'ih-cms-css', '--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'ih-cms-js', '--force' => true]);
            $this->exec([
                ['npm', 'install', '--save', 'autoprefixer'],
                ['npm', 'install', '--save', 'concurrently'],
                ['npm', 'install', '--save', 'sweetalert2'],
                ['npm', 'install', '--save', 'nanoid'],
                ['npm', 'install', '--save', 'lodash'],
                ['npm', 'install', '--save', 'vuedraggable'],
                ['npm', 'install', '--save', '@vueuse/core'],
                ['npm', 'install', '--save', '@vuelidate/core'],
                ['npm', 'install', '--save', '@vuelidate/validators'],
                ['npm', 'install', '--save', '@vitejs/plugin-vue'],
                ['php', '-r', 'file_exists(getcwd() . \'/.env\') || copy(getcwd() . \'/.env.example\', getcwd() . \'/.env\');'],
                ['php', '-r', 'file_put_contents(getcwd() . \'/.env\', file_get_contents(getcwd() . \'/vendor/ihero/cms/.env.example\') . PHP_EOL, FILE_APPEND);'],
                ['php', '-r', 'file_put_contents(getcwd() . \'/.env.example\', file_get_contents(getcwd() . \'/vendor/ihero/cms/.env.example\') . PHP_EOL, FILE_APPEND);'],
            ]);
        }
    }

    public function exec(array $commands = [])
    {
        if (($count = count($commands)) > 0) {
            $this->comment('Package installation:');

            $bar = $this->output->createProgressBar($count);
            $bar->start();

            foreach ($commands as $command) {
                $this->execing($command);
                $bar->advance();
            }

            $bar->finish();
        }
    }

    protected function execing(array $command = [])
    {
        $process = new Process($command);
        try {
            $process->mustRun();
        } catch (ProcessFailedException $exception) {
            $this->error($exception->getMessage());
        }
    }
}
