<?php

namespace App\Support\Uploader;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MediaUploader
{
    /**
     *
     * @param Array|null $files
     * @return array
     */
    public function batch(?Array $files, Array $size = []): array
    {
        if (filled($files)) {
            return array_map(function ($file) use ($size) {
                return $this->upload($file, $size);
            }, $files);
        }
        return [];
    }

    /**
     *
     * @param UploadedFile|Null $file
     * @return array
     */
    public function upload(?UploadedFile $file, Array $size = []): array
    {
        if (filled($file)) {
            $uuid = Str::uuid();

            if (Str::is('image/*', $file->getMimeType())) {
                $resource = $this->clipping($file, $size);
                Storage::put(sprintf('medias/%s', $uuid), $resource);
            } else {
                Storage::putFileAs('medias', $file, $uuid);
            }

            return [
                'uuid' => $uuid,
                'name' => $file->getClientOriginalName(),
                'type' => $file->getMimeType(),
                'url' => Storage::url(sprintf('medias/%s', $uuid))
            ];
        }
        return [];
    }

    /**
     *
     * @param UploadedFile $file
     * @param Array $size
     * @return String
     */
    protected function clipping(UploadedFile $file, Array $resize)
    {
        if (filled($resize)) {
            $image = Image::make($file);
            if (Arr::has($resize, 'clipping') && $resize['clipping'] =='ratio') {
                $image->resize($resize['width'], $resize['height'], function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image->fit($resize['width'], $resize['height'], function ($constraint) {
                    $constraint->upsize();
                });
            }

            return $image->stream();
        }
    }
}
