<?php

namespace App\Support\Uploader;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class FileUploader
{
    /**
     * Undocumented function
     *
     * @param UploadedFile|Null $file
     * @return void
     */
    public function upload(?UploadedFile $file)
    {
        if (filled($file)) {
            $uuid = Str::uuid();

            Storage::putFileAs('files', $file, $uuid);

            return [
                'uuid' => $uuid,
                'name' => $file->getClientOriginalName(),
                'type' => $file->getMimeType()
            ];
        }
        return [];
    }
}
