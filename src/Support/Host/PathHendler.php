<?php

namespace Ihero\CMS\Scaffold\Support\Host;

use Illuminate\Support\Str;

trait PathHendler
{
    /**
     *
     * @param String $path
     * @return String
     */
    public function path(String $path)
    {
        return sprintf('%s%s', $this->url(), Str::of($path)->start('/')->replace('//', '/'));
    }
}
