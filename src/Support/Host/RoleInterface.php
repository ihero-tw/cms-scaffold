<?php

namespace Ihero\CMS\Scaffold\Support\Host;

interface RoleInterface
{
    public function url();
}
