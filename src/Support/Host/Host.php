<?php

namespace Ihero\CMS\Scaffold\Support\Host;

use Illuminate\Support\Str;

class Host
{
    /**
     * Role variable
     *
     * @var String
     */
    protected $host;

    /**
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        //
    }

    /**
     * Call the Repository magic method.
     *
     * @param String $method
     * @param Array $args
     * @return Mixed
     */
    public function __call(String $method, Array $args)
    {
        if (! class_exists($class = sprintf('App\\Support\\Host\\Roles\\%s', Str::studly($method)))) {
            $class = sprintf('Ihero\\CMS\\Host\\Roles\\%s', Str::studly($method));
        }

        if (class_exists($class)) {
            return new $class($args[0] ?? null);
        }

        return ;
    }
}
