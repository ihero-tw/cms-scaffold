<?php

namespace Ihero\CMS\Scaffold\Support\Host\Roles;

use App\Support\Host\Role;
use App\Support\Host\RoleInterface;
use App\Support\Host\PathHendler;

class Admin extends Role implements RoleInterface
{
    use PathHendler;

    /**
     *
     * @return void
     */
    public function url()
    {
        return sprintf('https://%s', config('app.domains.admin'));
    }
}
