<?php

namespace Ihero\CMS\Scaffold\Support\Host\Roles;

use App\Models\Site;
use App\Support\Host\Role;
use App\Support\Host\RoleInterface;
use App\Support\Host\PathHendler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Web extends Role implements RoleInterface
{
    use PathHendler;

    /**
     *
     * @return void
     */
    public function url()
    {
        if (! Str::contains(request()->root(), config('app.domain'))) {
            return request()->root();
        }

        if ($this->model instanceof Site) {
            return sprintf('https://%s.%s', $this->model->uuid, config('app.domain'));
        } elseif (Auth::organization() instanceof Site) {
            return sprintf('https://%s.%s', Auth::organization()->uuid, config('app.domain'));
        }

        return null;
    }
}
