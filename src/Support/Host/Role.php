<?php

namespace Ihero\CMS\Scaffold\Support\Host;

use Illuminate\Database\Eloquent\Model;

class Role
{
    protected $model = '';

    /**
     *
     * @param Transmitter $role
     */
    public function __construct(?Model $model)
    {
        $this->model = $model;
    }
}
