import './bootstrap'
import '../../css/app.frontend.css'

import builtIn from '@/BuiltIn/register'
import { createSSRApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers'
import { ZiggyVue } from '../../../vendor/tightenco/ziggy/dist/vue.m'

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel'
const appPage = window.appPage = document.body.getAttribute('data-page')

createInertiaApp({
  id: 'inertia-app',
  title: (title) => `${title} - ${appName}`,
  resolve: (name) => resolvePageComponent(`./Pages/${appPage}/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
  setup({ el, App, props, plugin }) {
    return createSSRApp({ render: () => h(App, props) })
      .use(plugin)
      .use(builtIn)
      .use(ZiggyVue, Ziggy)
      .mount('#app')
  },
  progress: {
    color: '#6610f2',
  }
})
