import _ from 'lodash'
import { usePage } from '@inertiajs/vue3'
import { watch, reactive, unref } from 'vue'

/**
 * 取得 props 資訊的通用方法
 * @param {*} name props 資料名稱
 * @returns
 */
export default function getRepositories (name) {
  const repositories = reactive({})
  const variable = () => {
    const repos = usePage().props[name]
    if (_.every(['data', 'links', 'meta'], (v) => _.includes(_.keys({ ...repos }), v))) {
      return repos
    }
    return repos?.data ?? repos
  }
  const assignRepos = () => {
    let repos = variable()
    if (!_.isUndefined(repos) && !_.isNull(repos)) {
      Object.assign(repositories, { ...repos })
    }
  }

  watch(() => usePage().props[name], () => {
    assignRepos()
  }, { deep: true })
  assignRepos()

  return repositories
}
