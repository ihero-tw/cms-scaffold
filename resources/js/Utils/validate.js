function invalid(input, type) {
  if (input.$errors[0]) {
    if (input.$errors[0].$validator == type) {
      return true
    }
  }
  return false
}

function invalidEach(input, type) {
  if (input.length) {
    if (input[0].$validator == type) {
      return true
    }
  }
  return false
}

function isDateTimeString(dateString) {
  return new Date(dateString) instanceof Date && !isNaN(new Date(dateString));
}

function vInvalidClass ($v, errorClass = 'is-invalid', successClass = '') {
  return {
    [errorClass]: $v.$error,
    [successClass]: !$v.$invalid,
  };
}

export { invalid, invalidEach, isDateTimeString, vInvalidClass };
