import _ from 'lodash'
import { reactive } from 'vue'

export function getMediaUploadParameters (maximum = 1, size = (1024 * 1024) * 5) {
  const getInitialFileError = () => {
    return {
      type: false,
      size: false,
      maximum: false
    }
  }

  const fileError = reactive({ ...getInitialFileError() })
  const filter = (newFile, oldFile, prevent) => {
    Object.assign(fileError, getInitialFileError())

    if (newFile && !oldFile) {
      if (!/\.(jpeg|jpe|jpg|gif|png|webp)$/i.test(newFile.name)) {
        fileError.type = true
        return prevent()
      }
      if (newFile.size > size) {
        fileError.size = true
        return prevent()
      }
      if (maximum.value < 1) {
        fileError.maximum = true
        return prevent()
      }
    }

    if (newFile) {
      newFile.blob = ''
      let URL = window.URL || window.webkitURL
      if (URL && URL.createObjectURL) {
        newFile.blob = URL.createObjectURL(newFile.file)
      }
    }
  }

  return { fileError, filter }
}

export function getFileUploadParameters () {
  //
}

export function removePhoto (ref, photo) {
  ref.remove(photo)
}
