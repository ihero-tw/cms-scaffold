/**
 * input 格式化指定型態
 * 範例 : @input="old=integerOnly(state.model.price, 'original', old, 1, 1000000)"
 * @param {*} model 資料 model
 * @param {*} key 資料 key
 * @param {*} old 前一個數值
 * @param {*} min 最小值
 * @param {*} max 最大值
 */
function integerOnly(model, key, old=0, min=0, max=100) {
  let input = parseInt((model[key]).replace(/\D/g,''))
  if (isNaN(input) || input == 0) {
    model[key] = 0
    return 0
  }
  input = (min <= input && input <= max) ? input : old
  model[key] = input
  return input
}

export { integerOnly, lenghLimit, telLimit };

/**
 * input 格式化指定型態
 * 範例 : @input="old=lenghLimit(form, 'original', old, 1, 1000000)"
 * @param {*} model 資料 model
 * @param {*} key 資料 key
 * @param {*} old 前一個數值
 * @param {*} min 最小長度
 * @param {*} max 最大長度
 */
function lenghLimit(model, key, old='0', max='10') {
  let input = (model[key]).toString().replace(/\D/g,'')

  input = input.length <= max ? input : old
  input = input.length <= max ? input : old

  model[key] = input

  return input
}

/**
 * input 格式化指定型態
 * 範例 : @input="old=telLimit(form, 'original', old, 1, 1000000)"
 * @param {*} model 資料 model
 * @param {*} key 資料 key
 * @param {*} old 前一個數值
 * @param {*} min 最小長度
 * @param {*} max 最大長度
 */
function telLimit(model, key, old='0', max='10') {
  let input = (model[key]).toString().replace(/[^0-9-#]/g,'')

  input = input.length <= max ? input : old
  input = input.length <= max ? input : old

  model[key] = input

  return input
}

