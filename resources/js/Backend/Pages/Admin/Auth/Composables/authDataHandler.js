import { router } from '@inertiajs/vue3'
import { required, email } from "@vuelidate/validators"
import { computed, reactive } from 'vue'

export function getStateAndRules () {
  const state = reactive({
    email: '',
    password: '',
    remember: false
  })

  const rules = computed(() => {
    return {
      email: {
        email,
        required
      },
      password: {
        required
      }
    }
  })

  return { rules, state }
}

export function getFormFunctions(v$, state, passwordType) {
  return {
    submit: function () {
      v$.value.$touch()
      if (!v$.value.$invalid) {
        router
          .form(state)
          .post(route('admin.login'), {
            preserveState: false
          })
      }
    },
    switchVisibility: function () {
      passwordType.value =
      passwordType.value === "password" ? "text" : "password"
    }
  }
}
