import { usePage } from '@inertiajs/vue3'
import { ref, watch } from 'vue'

export function useButtonFunctions(props) {
  return {
    isChanged: () => {
      const changed = ref(false)
      watch(() => props.state, () => changed.value = true , { deep: true })
      watch(() => props.check, (v) => changed.value = v, { deep: true })
      watch(() => usePage().props , () => {
        if (!props.check) {
          return changed.value = false
        }
      } , { deep: true })
      return changed
    }
  }
}
