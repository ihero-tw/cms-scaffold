import { usePage } from '@inertiajs/vue3'
import { ref, watch } from 'vue'

export default function getMenuRepositories () {
  const menu = ref({})
  menu.value = usePage().props.menu?.data

  watch(usePage().props, () => {
    menu.value = usePage().props.menu?.data
  })

  return menu
}
