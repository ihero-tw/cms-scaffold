import { usePage } from '@inertiajs/vue3'
import { ref, watch } from 'vue'

export default function getNavbarRepositories () {
  const navbar = ref({})
  navbar.value = usePage().props.navbar?.data

  watch(usePage().props, () => {
    navbar.value = usePage().props.navbar?.data
  })

  return navbar
}
