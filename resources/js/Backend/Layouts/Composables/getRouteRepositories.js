import { usePage } from '@inertiajs/vue3'
import { ref, watch } from 'vue'

export default function getRouteRepositories () {
  const route = ref({})
  route.value = usePage().props.route

  watch(usePage().props, () => {
    route.value = usePage().props.route
  })

  return route
}
