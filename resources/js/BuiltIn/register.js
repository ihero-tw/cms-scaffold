import currency from './Directives/currency'
import breadcrumb from './Directives/breadcrumb'
import remoteScript from './Components/remoteScript'
import Modal from './Components/Modal/Index.vue'

// register data list
const installComponentData = {
  'directive': [
    { 'name': 'currency', 'component': currency },
    { 'name': 'breadcrumb', 'component': breadcrumb }
  ],
  'component': [
    { 'name': 'RemoteScript', 'component': remoteScript },
    { 'name': 'Modal', 'component': Modal },
  ]
}

// register all components
export default {
  install: (app) => {
    Object.keys(installComponentData).forEach(dataItem => {
      installComponentData[dataItem].forEach(item => {
        if (['use', 'mixin'].indexOf(dataItem) >= 0) {
          app[dataItem](item.component)
        } else {
          app[dataItem](item.name, item.component)
        }
      })
    })
  }
}
