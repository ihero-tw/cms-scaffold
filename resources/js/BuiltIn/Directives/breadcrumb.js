import _ from 'lodash'
import { usePage } from '@inertiajs/vue3'
import { watch, ref, isRef, reactive } from 'vue'
import getRepositories from '@/Utils/repositories'

/**
 * get breadcrumb label from menu
 * @param {*} name props 資料名稱
 * @param {*} type ref or reactive
 * @returns
 */
export default (el, binding, vnode) => {
  const menu = getRepositories('menu')
  Object.filter = (obj, predicate) =>
    Object.keys(obj)
      .filter( key => predicate(obj[key]) )
      .reduce( (res, key) => (res[key] = obj[key], res), {} )

  if (binding.arg == 'backend') {
    el.innerHTML =  Object.values(Object.filter(menu, data => data.active))[0].label + ' /' + el.innerHTML
  } else if (binding.arg == 'frontend') {
    el.innerText = 'frontend breadcrumb'
  }
}
