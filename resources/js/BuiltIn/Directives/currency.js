import currency from 'currency.js'

export default (el, binding, vnode) => {
  if (binding.arg == 'smallsymbol') {
    el.innerHTML = currency(binding.value, {
      precision: 0,
      symbol: '<small>$</small>'
    }).format()
  } else if (binding.arg == 'nosymbol') {
    el.innerText = currency(binding.value, {
      precision: 0,
      symbol: ''
    }).format()
  } else {
    el.innerText = currency(binding.value, {
      precision: 0,
      symbol: '$'
    }).format()
  }
}
