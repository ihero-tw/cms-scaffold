import { defineComponent, createElementVNode } from 'vue'

export default defineComponent({
  render: function () {
    return (typeof window === "object")? createElementVNode('script', {
      type: 'text/javascript',
      src: this.src
    }): null
  },

  props: {
    src: {
      type: String,
      required: true
    }
  }
})
