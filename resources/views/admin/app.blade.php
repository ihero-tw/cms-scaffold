<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light-style">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title inertia>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link
    href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
    rel="stylesheet"
  />

  @routes
  @vite([
    "resources/themes/sneat/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css",
    "resources/themes/sneat/assets/vendor/fonts/boxicons.css",
    "resources/themes/sneat/assets/vendor/css/core.css",
    "resources/themes/sneat/assets/vendor/css/theme-default.css",
    "resources/themes/sneat/assets/vendor/css/pages/page-auth.css",
    "resources/themes/sneat/assets/css/demo.css",
    "resources/themes/sneat/assets/vendor/js/helpers.js",
    "resources/themes/sneat/assets/js/config.js",
    "resources/themes/sneat/assets/vendor/libs/jquery/jquery.js",
    "resources/themes/sneat/assets/vendor/libs/popper/popper.js",
    "resources/themes/sneat/assets/vendor/js/bootstrap.js",
    "resources/themes/sneat/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js",
    "resources/themes/sneat/assets/vendor/js/menu.js",
    "resources/js/Backend/app.js",
    "resources/js/Backend/Pages/{$page['props']['route']['app']}/{$page['component']}.vue"
  ])
  @inertiaHead
</head>

<body data-page="Admin">
  @inertia

  <script async defer src="https://buttons.github.io/buttons.js"></script>
</body>

</html>
