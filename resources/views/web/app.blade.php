<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

        <!-- Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>

        <!-- Scripts -->
        @routes
        @vite([
          "resources/themes/material-kit/assets/css/nucleo-icons.css",
          "resources/themes/material-kit/assets/css/material-kit.min.css",
          "resources/themes/material-kit/assets/js/core/popper.min.js",
          "resources/themes/material-kit/assets/js/core/bootstrap.min.js",
          "resources/themes/material-kit/assets/js/plugins/perfect-scrollbar.min.js",
          "resources/themes/material-kit/assets/js/plugins/choices.min.js",
          "resources/js/Frontend/app.js",
          "resources/js/Frontend/Pages/{$page['props']['route']['app']}/{$page['component']}.vue"
        ])
        @inertiaHead
    </head>
    <body class="index-page bg-gray-200" data-page="Web">
      <div id="app"></div>
      @inertia('inertia-app')
    </body>
</html>
