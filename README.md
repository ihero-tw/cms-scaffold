## CMS Scaffold

Ihero CMS

### 安裝方法

#### 在新 Laravel 專案中的執行以下指令

** 加入安裝來源
```
composer config repositories.ihero-cms vcs https://ihero-ajieeeee@bitbucket.org/ihero-tw/cms-scaffold.git
composer config repositories.ihero-generator vcs https://ihero-ajieeeee@bitbucket.org/ihero-tw/laravel-generator.git
```

** 加入套件
```
composer require ihero/cms
```

** 執行安裝
```
php artisan ih.cms:install
```

### 指令

```
    // 安裝 CMS 環境
    php artisan ih.cms:install

    // 安裝部分功能
    php artisan vendor:publish ih-cms-themes
    php artisan vendor:publish ih-cms-js
    php artisan vendor:publish ih-cms-views
    php artisan vendor:publish ih-cms-init
```
